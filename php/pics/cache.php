<?php

define('CACHE_DIR', dirname(__FILE__).'/cache');
if (!is_dir(CACHE_DIR)) mkdir(CACHE_DIR, 0777, true);

$file = $_GET['pic'];
$a = explode('/',$file);
if (count($a) == 1){
  $thema = '';
} else {
  $thema = $a[0];
  $file = $a[1];
}
if (isset($_GET['thema']))
  $thema = $_GET['thema'];

$w = $_GET['w'];
$h = $_GET['h'];


$srcFile = dirname(__FILE__).'/'.$thema.'/'.$file;

$path_info = pathinfo($srcFile);
$ext = $path_info['extension'];

$cacheFile = CACHE_DIR.'/'.$thema.'-'.$file.'-'.$w.'-'.$h;

ob_end_clean();

$mtime = filemtime($srcFile);

// create cache file:
if (!file_exists($cacheFile) || $mtime > filemtime($cacheFile)) {

	$size = getimagesize($srcFile);
	$isw = $size[0];
	$ish = $size[1];

	$img = file_get_contents($srcFile);
	if (!$img) {
		echo "$srcFile not found.\n";
		exit();
	}
	$src = imagecreatefromstring($img);

	$target = imagecreatetruecolor($w, $h);

        // preserve transparency
        if(in_array($ext, array('gif','png'))){
          imagecolortransparent($target, imagecolorallocatealpha($target, 0, 0, 0, 127));
          imagealphablending($target, false);
          imagesavealpha($target, true);
        }

        # imagesavealpha ( $target , true);
        # $transparent = imagecolorallocatealpha($target, 0, 0, 0, 127);
        # imagefill($target, 0, 0, $transparent);
        # imagecolordeallocate($target, $transparent);

	imagecopyresampled($target, $src, 0, 0, 0, 0, $w, $h, $isw, $ish);

	// Output
        if ($ext == 'png'){
          imagepng($target, $cacheFile);
        } else {
          imagejpeg($target, $cacheFile);
        }
	imagedestroy($src);
	imagedestroy($target);
}

function conditionalGET($timestamp) {
  $last_modified = gmdate("D, d M Y H:i:s \G\M\T", $timestamp);
  $etag = '"'.md5($last_modified).'"';
  header("Last-Modified: $last_modified");
  header("ETag: $etag");
  $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
  $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : false;
  if(!$if_modified_since && !$if_none_match) return;
  if($if_none_match && $if_none_match != $etag) return;
  if($if_modified_since && $if_modified_since != $last_modified) return;
  header("HTTP/1.0 304 Not Modified");
  exit;
}

// conditionalGET2(filemtime($cacheFile));
// conditionalGET($mtime);

switch ($ext) {
  case 'png':
    header('Content-type: image/png');
    break;
  case 'jpg':
    header('Content-type: image/jpeg');
	break;
  default:
    throw new Exception("bad extension");
}

$contents = file_get_contents($cacheFile);
header("Content-Length: ".strlen($contents));
echo $contents;
