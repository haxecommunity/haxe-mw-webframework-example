import SPODs;
import Cfg;
import Email;
using mw.StringExtensions;

class EmailVerification {

  static public var data_salt = "qz:EmgFrW6";

  // prevent loading this class for this method, thus separate
  // uri detection from implementation
  static public inline function handle(ctx:Ctx) {
	var r = ~/email-verification\/([^\/]*)$/;
	if (r.match(ctx.uri_rest)){
	  var data = r.matched(1);
	  var x = Verification.manager.search($data == data, {limit: 1}).first();
	  if (x == null){
		HTML.msg(ctx, ctx.translate("verification failed", ""));
	  } else {
		x.verification_date = Date.now();
		x.update();
		HTML.msg(ctx, ctx.translate("email verification complete",""));
	  }
	  return true;
	} else return false;
  }

  static public function verify(ctx:Ctx) {
  }

  static public function sendVerificationEmail(ctx:Ctx, user:User) {
	var v = new Verification();
	var data = haxe.crypto.Md5.encode(data_salt+user.email+Math.random());
	v.user_id = user.user_id;
	v.data = data;
	v.insert();

	var m = new Email();
	m.to = [user.email];
	m.subject = ctx.translate('${Cfg.projectName}: verify signup', "");
	m.content = text(ctx.translate("
		Liebe(r) _firstname _lastname,

		Bitte klicke auf den untenstehenden Link um die Anmeldung abzuschließen:
		_link

	".replaceAnon({
	  _firstname:  user.firstname,
	  _lastname: user.lastname,
	  _link: ctx.http_uri_prefix+'/email-verification/${data}'
	}),
	"email verification email content"
    ));
	ctx.send_email(m);
  }


}
