import js.html.Element;
import Cfg;
import jQuery.*;
import pc.FormLoginOrCreateAccount;
#if DEBUG
import pc.Form
#end
using Reflect;

@:expose("JSMain")
class JSMain {

  public static var ctx: Ctx;

  static public function enhance(e: Element) {
    pc.JSEnhance.enhance(e);
  }

  static function main() {
    JSMain.ctx = {
      translate: function(a:String, b:String){ return  a; },
      http_uri_prefix: untyped js.Browser.window.data.http_uri_prefix,
      localization: {
        date_to_str: function(d:Date){
          return DateTools.format(d, '%Y-%m-%d');
        },
        datetime_to_str: function(d:Date, secs:Bool = false){
          return DateTools.format(d, '%Y-%m-%d %H:%m}');
        },
        time_to_str: function(d:Date, secs:Bool = false){
          return DateTools.format(d, '%H:%m');
        }
      }
    };
    new JQuery(function():Void {
      //when document is ready
      var body = js.Browser.document.body;
      enhance(body);
      new JQuery(js.Browser.window).resize(function(event):Void{
        pc.JSEnhance.resize(body);
      });
      pc.JSEnhance.resize(body);
    });
  }

}
