class Password {

  static var salt: String = "uT:eqo1F";

  static public function code(s:String) {
	return haxe.crypto.Md5.encode(salt + s);
  }

}
