// import app.schemes.AppScheme;
import tink.lang.Cls;
import Image;

import haxe.macro.Expr;
import haxe.macro.Context;

using mw.Assertions;
using Lambda;

typedef AppConfig = {
  // you can add more fields here (however its you who must ensure that those
  // fields are in app-config.json .. !
  dev_error_file: String,

  domain_and_path:String, // something like localhost/path

  mysql_connection: {
       user : String,
       socket : String,
       port : Int,
       pass : String,
       host : String,
       database : String 
    }
  };

typedef Ctx = {
#if SERVER
  response: web.ResponseState,
  request: web.RequestState,

  uri_rest: String, // rest of uri (the path identifying the page), without leading /
  uri_rest_without_get: String, // rest of uri (the path identifying the page), without leading /

#if !macro
  send_email: Email -> Void,

  image_store: ImageStore, // path, image contents, returning dimensions
  image_url:  ImageUrl,
  image_html_tag: ImageHtmlTag,
#end


#end

  // localization
  localization : {
    date_to_str : Date -> String,
    datetime_to_str: Date -> Bool -> String, // without second
    time_to_str: Date -> Bool -> String, // without second
  },

  // translating a string:, language will be derived from url
  translate: TranslationFunc,

  http_uri_prefix: String, // http://domain/base-dir of website (with language),
};


#if LOG_SQL
class LoggingDb implements Cls {
  @:forward(close, escape, quote, addValue, lastInsertId, dbName, startTransaction, commit, rollbacek) var con:sys.db.Connection;

  public function new(con) {
    this.con = con;
  }

  public function request(s:String) {
    return this.con.request(s);
  }
}
#end


class Cfg {

  static public var password_strength:Int = 4;
  static public var label_width:Int = 250;

#if SERVER
  public static var email_from:String = "test@test.de"; // FIXME

  public static var projectName = "DEMO APP"; // FIXME

  static public var admin_emails = ['you@test.de']; // FIXME

  // all pc.PC classes which may have ajax like calls using 
  public static var pc_classes = [
    "pc.FormLoginOrCreateAccount"
  ];

  public static var app_config: AppConfig;
#if SERVER
  public static var sys_db: sys.db.Connection;
#end

  // cannot use __init__, mysql fails to connect
  public static function setup() : Void {
    var config_file = "app-config.json";
    app_config = haxe.Json.parse(sys.io.File.getContent(config_file));

    #if !macro
    sys_db = sys.db.Mysql.connect(app_config.mysql_connection);

#if LOG_SQL
    sys_db = cast(new LoggingDb(sys_db));
#end

    // connection_interface = new xx.db.StdConnection(sys_db);
    #end
  }

#if macro
  static public var update_migrations = true;
#end

#end

  // from field type return new Input({...})
  macro static public function component(table:String, field:String):Expr {
    var s = app.schemes.AppScheme.scheme();
    var t = mw.relational.SchemeExtensions.tableByName(s, table).assert_nn('table ${table} not found');
    var f = mw.relational.SchemeExtensions.fieldByName(t, field).assert_nn('field ${field} in table ${table} not found');
    if (f.pc_form_content == null){
        var fields = new Map<String, { field : String, expr : Expr }>();
        var pos = Context.currentPos();
        function sf(name, e:Expr){
          fields.set(name, { expr : e, field: name});
        }
        function fe(){ return {pos: pos, expr: EObjectDecl(fields.array()) }; }
        // defaults
        sf("ctx", macro ctx);
        sf("name", macro $v{f.name});
        sf("label", (f.pc_form_label != null) ? (macro translate($v{f.pc_form_label}, "")) : macro translate($v{f.name}, "") );
        if (f.check != null)
          sf("check", f.check);
        if (f.pc_form_default != null)
          sf("default_", f.pc_form_default);
        if (f.pc_form_type != null)
          sf("type", f.pc_form_type);
        // hint
        if (f.hint_on_form != null)
          sf("hint_on_form", macro $v{f.hint_on_form });

        /*
          name:String,
          label:String,
          ctx: Ctx,
          type: InputType,
          ?check: check.CheckFunction,
          ?html_tags: Dynamic, // { style="....", etc ... }
          ?default_: Dynamic,
          ?hint:String,
          ?hint_on_form: String
          */

        f.pc_form_content = switch (f.type_) {
          case text(length):
            if (!fields.exists("check"))
              sf("check", macro CheckImplementations.str_length(0, $v{length}) );
            sf("type", (length > 255 )
                        ? macro textarea({cols: 60, rows: 10})
                        : macro text);
            macro new Input(${fe()});
          case int:
            if (!fields.exists("check"))
              sf("check", macro CheckImplementations.is_int() );
            sf("type", macro text);
            // TODO check function
            macro new Input(${fe()});
          case float(precision):
            macro new Input(${fe()});
          case blob:
            macro new Input(${fe()});
          case bool_spod:
            sf("type", macro checkbox);
            macro new Input(${fe()});
          case enum_(values):
            if (!fields.exists("type")){
              var x = [];
              for (c in values)
                x.push(macro {key: $v{c},  value: $v{c}} );
              sf("type", macro select(${{pos: pos, expr: EArrayDecl(x)}}));
            }
            macro new Input(${fe()});
          case haxetype(type_as_string, size):
            throw "TODO";
            //macro new Input(${fe()});
          case date:
            if (!fields.exists("type"))
              sf("type", macro date({}));
            macro new Input(${fe()});
          case datetime:
            if (!fields.exists("type"))
              sf("type", macro datetime(false));
            macro new Input(${fe()});
          case currency:
            throw "TODO";
            macro new Input(${fe()});
#if SUPPORT_RELATIONAL_SPECIAL_FIELDS
          case image_on_disk(categor, len):
            if (!fields.exists("type"))
              sf("type", macro file);
            macro new Input(${fe()});
          case email(len): // holds an email
            if (!fields.exists("type"))
              sf("type", macro text);
            if (!fields.exists("check"))
              sf("check", macro CheckImplementations.email());
            macro new Input(${fe()});
          case password(len): // holds a password
            if (!fields.exists("type"))
              sf("type", macro password);
            if (!fields.exists("check"))
              sf("check", macro CheckImplementations.password(6));
            macro new Input(${fe()});
#end
      }

    }
    return f.pc_form_content;
  }
}
