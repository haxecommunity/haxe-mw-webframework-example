import app.schemes.AppScheme;
import Cfg;

using web.RequestResponseHelper;

class Maintainance {

  static public function automigrate(rr:Ctx) {
    var v = mw.migrate.Migrate.migrateMySQL(Cfg.sys_db, "src/migrations", "migrations", "version", "app.schemes.AppScheme.mysqlScheme()");
    rr.end(200, 'scheme version is ${v}');
  }

  static public function loadLanguages(rr) {

    rr.end(200, "languages loaded");
  }

}
