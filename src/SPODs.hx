import sys.db.Types;

import Cfg;
import Image;

import web.RequestResponseHelper;

using Reflect;
#if SERVER

#if !macro

#if !macro @:autoBuild(mw.relational.SPODBuilder.build("app.schemes.AppScheme.scheme()", "MySQL")) #end interface SPODBuilder {
}

@:table("people")
@:id(people_id)
class People extends sys.db.Object implements SPODBuilder  {
}


@:table("users")
@:id(user_id)
class User extends sys.db.Object implements SPODBuilder  {
}

@:table("verifications")
@:id(verification_id )
class Verification extends sys.db.Object implements SPODBuilder {
}

#end

#end
