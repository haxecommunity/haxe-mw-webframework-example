package app.schemes;

import mw.relational.FieldType;
import mw.relational.Scheme;
import mw.mysql.scheme.Scheme;

enum AttendeeStatus {
  denied;
  approved;
  unkown;
}

class AppScheme {

#if macro

  public static var scheme_cache: mw.relational.Scheme = null;
  static public function scheme(): mw.relational.Scheme {

    if (scheme_cache == null){

      var f : mw.relational.LazyField ->  mw.relational.LazyField = function(x)return x;
      var s: mw.relational.Scheme = {

        mToN_relations: [
          // // transactions
          { tableName: "is_friend_of", m: "people", n: "people",
            m_fields: ["person"],
            n_fields: ["is_friend_of"],
            fields: [
              { name: "rating", type_: int },
            ]
          },

        ],
            // transactions
        tables: [

          ({
            name: "cities",
            fields: [
              {name: "name", type_: text(200) },
            ]

          }),

          ({
            name: "people",
            fields: [
              {name: "name", type_: text(200) },
            ],
            parents: [
              {table: "cities", forceParent: false }
            ]

          }),


          ({
            name: "users",
            fields: [
              {name: "email", type_: email(200) },
              {name: "username", type_: text(200) },
              {name: "firstname", type_: text(200) },
              {name: "lastname", type_: text(200) },
              {name: "password", type_: password(200) },
              {name: "password_reset_code", type_: text(150)},
              {name: "picture", type_: image_on_disk("user_image", 40) }
            ]

          }),

          ({
            name: "verifications",
            parents: [
              {table: "users", forceParent: true}
            ],
            fields: [
              { name: "name", type_: enum_(["email", "newsletter"]) },
              { name: "verification_date", type_: datetime, nullable: true },
			  { name: "data", type_: text(200) },
			  { name: "updated", type_: datetime, mysql_on_update_current_timestamp: true }
            ],
          }),
        ]
      }

      mw.relational.SchemeExtensions.complete(s);
      scheme_cache = s;
    }
    return scheme_cache;
  }

  public static var mysql_scheme_cache: mw.mysql.scheme.Scheme = null;

  static public function mysqlScheme(): mw.mysql.scheme.Scheme {
    if (mysql_scheme_cache == null){
      mysql_scheme_cache = mw.relational.SchemeExtensions.toMySQLScheme(scheme());
    }
    return mysql_scheme_cache;
  }

#end

}
