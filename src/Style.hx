import Cfg;
using web.RequestResponseHelper;

class Style {
	static var dark_brown = "#52423b";
	static var pink = "#ed0f67";
	static var light_brown = "#706864";
	static var color_hover = "#706864";
	static var color_label = "#52423b";
	static var color_error = "#FFcccc";
    static var color_bar_brands = "#2a2a2a";
    static var color_gray = "#efefef";

	static public function style(ctx: Ctx) {
		ctx.end(200, '

.message {
  background-color: #FFF;
  ${round_corner(4)}
}

#page_container {
  max-width: 1200px;
  margin: auto;

  font-family: \'Playfair Display SC\', serif;
}

#background {
  background: url(${KnownImages.image_url(ctx,"bg.png","w-1200")}) no-repeat;
  min-height: 800px;
  position: relative;
}

#start_page_container {
  margin: 10px auto;
}


.form_label {
  width: ${Cfg.label_width}px;
  display: block;
  float: left;
}
.form_field {
  display: block;
  float: left;
}

.clear {
  clear:both;
}

.image_box {
  padding: 10px;
  display: inline-block;
}

.backlink_box {
  width; 220px;
  height: 240px;
  display: inline-block;
  padding: 10px;
  background-color: #FFF;
  ${round_corner(4)}
  border: 2px solid #500;
}

#fashion_lovers_list a {
  text-decoration: none;
  color: 000;
}

#start_page_container_bg {
  position: absolute;
  top: 0px;
  right: 0px;
  left: 0px;
  bottom: 0px;
  opacity: 0.8;
  background-color: #FFF;
  margin: 10px;
}
#start_page_content {
  z-index: 1;
  position: relative;
  padding-top: 20px;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 250px;
}

.center{
  text-align: center;
}
.form {
  max-width: 600px;
  margin: auto;
}

.bar {
  background-color: #FFF;
  height: 5px;
  width: 90%;
  margin: auto;
  margin-top: 10px;
  margin-bottom: 10px;
}

#bar_brands {
  position: absolute;
  z-index: 10;
  bottom: 0px;
  left: 0px;
  right: 0px;
  margin: 20px;
  background-color: ${color_bar_brands};

  padding: 20px;
}

#bar_brands a {
  color: #FFF;
  text-decoration: none;
}

.brand_image {
  margin: 20px;
  display: inline;
}

#login_page_login_form {
  margin: 10px auto;
}

.form_label {
  display: inline-block;
  width: 13em;
  color: ${color_label};
}

body {
  background-color: ${light_brown};
  margin: 0px;
  padding: 0px;
}

#navigation {
  background-color: ${dark_brown};
  padding: 0px;
  margin: 0px;
}

.navigation_button {
  color: ${pink};
  display: inline-block;
  margin: 5px;
  text-align: center;
  padding-left: 7px;
  padding-right: 7px;
  padding-top: 5px;
  padding-bottom: 5px;
  margin-top: 0px;
  margin-bottom: 0px;
}

.navigation_button.active {
  background-color: ${color_hover};
}

.navigation_button:hover {
  background-color: ${color_hover};
}

input, select, textarea, button {
  padding: 5px;
  ${round_corner(3)}
}
textarea {
  width: 100%;
}

input.error, select.error, textarea.error {
  background-color: ${color_error};
}

input[type=date] {
}

.form_horizontal_item {
margin: 5px;
}

.form {
  padding: 20px;
  margin-bottom: 5px;
}

h2 {
  text-decoration: underline;
}


.anmelde_flavour {
  margin-top: 10px;
  border-top: 1px solid #FFF;
  border-left: 1px solid #FFF;
}
');
  }

  // 1px 1px 0px #000
  static public function textShadow(arg) {
	return 'text-shadow: ${arg}';
  }

  // 3px 3px 5px 6px #ccc
  static public function shadow(s:String) {
	return '
	  -moz-box-shadow:    ${s};
	  -webkit-box-shadow: ${s};
	  box-shadow:         ${s};
	';
  }

  static public function round_corner(p:Int) {
	return '
	  -webkit-border-radius: 4px;
	  -moz-border-radius: 4px;
	  border-radius: 4px;
	';
  }

}
