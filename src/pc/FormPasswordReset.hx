package pc;
import Cfg;
import check.CheckImplementations;
import pc.PC;
import pc.Form;
import pc.form.Content;

import SPODs;

using DateTools;
using mw.StringExtensions;
using Reflect;

#if js
import jQuery.*;
#end

import pc.form.Input;

#if JS_CLIENT
@:keep
#end
class FormPasswordReset extends Form<{
  ?id:String,
  ctx:Ctx,
  user_id: Int
}> {

  public function new(o) {
    super(o);
    var translate = this.o.ctx.translate;

	this.formContent =
	  new pc.form.layout.Simple({
		elements: [
		  new pc.form.layout.Simple({ elements: [
			new Input({
				  name: "password",
				  default_: "",
				  ctx: o.ctx,
				  label: translate("password", ""),
				  type: password,
				  check: CheckImplementations.password(Cfg.password_strength)
				})
		  ]})
	    ]
	  });
  }

  override public function htmlForm() {
	var user_id = o.user_id;
	var u = User.manager.search($user_id == user_id, {limit: 1}).first();
	if (u == null)
      return mw.HTMLTemplate.str(".message=o.ctx.translate('something failed','')");

    var t = o.ctx.translate("send", "");
    return
      mw.HTMLTemplate.str("
      %h2=this.o.ctx.translate('Reset your password', '')
      !=errorBoxHTML()
      !=formContent.html()
      %button=t
    ");
  }

#if SERVER
  override public function action(): ActionResult {
    var user_id = o.user_id;
    var u = User.manager.search($user_id == user_id, {limit: 1}).first();
    u.password = Password.code(values.password);
    u.update();

    var message = o.ctx.translate("password has been reset successfully", "");

    var f = new pc.FormLoginOrCreateAccount({ctx: o.ctx});
    return ar_replace_html_of_dom_el({id: o.id,
        html: mw.HTMLTemplate.str("
      .message=message
      !=HTML.link(o.ctx,'startseite')
    ")});
  }
#end

}
