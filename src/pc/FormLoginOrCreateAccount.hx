package pc;
import Cfg;
import check.CheckImplementations;
import pc.PC;
import pc.Form;
import pc.form.Content;

import SPODs;

using DateTools;
using mw.StringExtensions;
using Reflect;
using mw.NullExtensions;
using web.RequestResponseHelper;

#if js
import jQuery.*;
#end

import pc.form.Input;

#if JS_CLIENT
@:keep
#end
class FormLoginOrCreateAccount extends Form<{
  ?id:String,
  ctx:Ctx,
}> {

  public var login_fields:Array<Content>;
  public var create_fields:Array<Content>;

  public function new(o) {
    super(o);
    this.js_instance = "id";
    var translate = this.o.ctx.translate;
    // this.layout = vertical_table;

    this.login_fields = [];
    this.create_fields = [];

    this.login_fields.push(new Input({
      name: "action",
#if SERVER
      default_: o.ctx.getOrNull("action").ifNull(""),
#end
      ctx: o.ctx,
      label: translate("action", ""),
      type: select([
		{ key: 'login', value: o.ctx.translate("login","") },
		{ key: 'sign_up', value: o.ctx.translate("sign up","") },
		{ key: 'reset_password', value: o.ctx.translate("reset password","") },
	  ]),
      check: null
    }));

    var ctx = o.ctx;

    this.login_fields.push(Cfg.component("users","email"));
    var pwd_field = Cfg.component("users","password");

    this.create_fields.push(Cfg.component("users", "username"));
    this.create_fields.push(Cfg.component("users", "firstname"));
    this.create_fields.push(Cfg.component("users", "lastname"));
    this.create_fields.push(Cfg.component("users", "picture"));

    this.formContent =
      new pc.form.layout.Simple({
            layout: list,
            elements: [
              new pc.form.layout.Simple({ elements: this.login_fields }),

              new pc.form.layout.OptionalContents({
                translate: o.ctx.translate,
                group_name: "pwd_field",
                active: function(values){
                  return values.field("action") != "reset_password";
                },
                formContent: new pc.form.layout.Simple({ elements: [pwd_field]})
              }),

              new pc.form.layout.OptionalContents({
                    translate: o.ctx.translate,
                    group_name: "signup_fields",
                    active: function(values){
                      return values.field("action") == "sign_up";
                    },
                    formContent: new pc.form.layout.Simple({ elements: this.create_fields })
              })
        ]
      });
  }

  override public function htmlForm() {
    var t = o.ctx.translate("send", "");
    return
      mw.HTMLTemplate.str("
      %h2=this.o.ctx.translate('Login', '')
      !=errorBoxHTML()
      !=formContent.html()
      %button=t
    ");
  }

#if SERVER

  override public function action(): ActionResult {
	switch values.action {
	case "reset_password":
	  var e:String = values.email;
	  var u = User.manager.search($email == e, {limit: 1}).first();
	  if (u == null)
		throw new ActionError(o.ctx.translate('Your email is unkown!', ''));
	  PasswordReset.sendPasswordResetMail(o.ctx, u);
	  var message = o.ctx.translate("You've been sent an email containing a password reset link","");
	  return ar_replace_html_of_dom_el({id: o.id, html: mw.HTMLTemplate.str("%p=message")});

	case "sign_up":

	  var e:String = values.email;
	  var u:String = values.username;

	  if (null != User.manager.search($username == u, {limit: 1}).first()){
		var error = o.ctx.translate('The username _USERNAME does already exist. Please choose a different one','').replaceAnon({
			  _USERNAME: u
		});
		errors.username = error;
		throw new ActionError(error);
	  }

	  if (null != User.manager.search($email == e, {limit: 1}).first()){
		var error = o.ctx.translate('The email _EMAIL has already been registered. Try resetting your password instead by changing the form action','').replaceAnon({
		  _EMAIL: e
		});
		errors.email = error;
		throw new ActionError(error);
	  }

	  // try creating a new user and send email confirmation mail
	  var u = new User();
          u.username = values.username;
          u.password = values.password;
          u.email = values.email;
          u.insert();
          u.picture_from_form(o.ctx.image_store, values.picture);
          u.update();

	  EmailVerification.sendVerificationEmail(o.ctx, u);

	  var message = o.ctx.translate("Your account has been created. Please click on the email verification link in the email you'll receive shortly","");

	  return ar_replace_html_of_dom_el({id: o.id, html: mw.HTMLTemplate.str("%p=message")});

	case "login":
	  // try logging in
	  var p:String = Password.code(values.password);
	  var e:String = values.email;
	  var u = User.manager.search($email == e && $password == p, {limit:1}).first();
	  if (u == null){
		errors.setField("email","!");
		errors.setField("password","!");
		throw new ActionError(o.ctx.translate("username or password wrong",""));
	  } else {
            // if (u.blocked_reason != null)
              // throw new ActionError(o.ctx.translate("You've been blocked: _MESSAGE","").replaceAnon({_MESSAGE:  u.blocked_reason}));
            WebSession.session().user = u;
          }
	  return ar_reload_page;
	}
	return null;
  }
#end

}
