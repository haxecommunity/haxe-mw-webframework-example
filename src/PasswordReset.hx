import SPODs;
import Cfg;
import Email;
using mw.StringExtensions;

using web.RequestResponseHelper;

class PasswordReset {

  static public var data_salt = Cfg.projectName + "ae$ntuh4";

  // prevent loading this class for this method, thus separate
  // uri detection from implementation
  static public inline function handle(ctx:Ctx) {
    var r = ~/password-reset\/([^\/]*)$/;
    if (r.match(ctx.uri_rest)){
      var data = r.matched(1);
      var u = User.manager.search($password_reset_code == data, {limit: 1}).first();
      if (u == null){
            HTML.msg(ctx, ctx.translate("bad code", ""));
      } else {
            // allow using the reset link only once
            u.password_reset_code = "";
            u.update();

            var f = new pc.FormPasswordReset({
              ctx: ctx,
              user_id: u.user_id
            });
            ctx.end(200, HTML.page(ctx, {
              title: ctx.translate("reset your password", ""),
              body: f.html()
            }));

      }
      return true;
    } else return false;
  }

  static public function verify(ctx:Ctx) {}

  static public function sendPasswordResetMail(ctx:Ctx, user:User) {
	var v = new Verification();
	var data = haxe.crypto.Md5.encode(data_salt+user.email+Math.random());

	user.password_reset_code = data;
	user.update();

	var m = new Email();
	m.to = [user.email];
	m.subject = ctx.translate("password reset", "");
	m.content = text(ctx.translate("
		Dear _firstname _lastname,

		You can reset your password by clicking on the following link:

		_link

	".replaceAnon({
	  _username:  user.firstname,
	  _lastname: user.lastname,
	  _link: ctx.http_uri_prefix+'/password-reset/${data}'
	}),
	"email verification email content"
    ));
	ctx.send_email(m);
  }
}
