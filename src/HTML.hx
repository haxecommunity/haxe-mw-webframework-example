import Cfg;
import Image;

import haxe.macro.Expr;
import haxe.macro.Context;

import SPODs;

using web.RequestResponseHelper;
using Reflect;
using mw.ArrayExtensions;


class HTML {

#if SERVER
  macro static public function link(ctx:ExprOf<Ctx>, name:String) {
    switch (name) {
      case "startseite":
        return macro $ctx.http_uri_prefix;
      case "bewerben":
        return macro $ctx.http_uri_prefix+"/bewerben";
      case "impressum":
        return macro $ctx.http_uri_prefix+"/impressum";
      case "kontakt":
        return macro $ctx.http_uri_prefix+"/kontakt";
      case "ueberuns":
        return macro $ctx.http_uri_prefix+"/ueberuns";
      case "fashion_lovers":
        return macro $ctx.http_uri_prefix+"/fashion_lovers";
      case "demoblog":
        return macro $ctx.http_uri_prefix+"/fashion_lovers";
      case "events":
        return macro $ctx.http_uri_prefix+"/events";
      // case "newsletter":
      //   return macro $ctx.http_uri_prefix+"/ueberuns";
      case _: throw 'bad link ${name}';
    }
  }

  static public var ctx: Ctx;
#if !macro

  static public function msg(ctx:Ctx, msg:String) {
    ctx.end(200, page(ctx, {
      title: 'message to user',
      body: mw.HTMLTemplate.str('.message=msg')
    }));
  }

  static public function page(ctx:Ctx, o:{
    title:String,
    body:String,
    ?description: String
  }) {

    if (o.description == null)
      o.description = "Demo application";

    return mw.HTMLTemplate.str('
      %html
        %head
          %meta(http-equiv="content-type" content="text/html; charset=UTF-8")
          %meta(http-equiv="content-type" content="text/html; charset=UTF-8")
          %meta(name="description" content=o.description)
          %title=o.title
          %link(rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" media="all")
          %link(rel="stylesheet" href=ctx.http_uri_prefix+"/style.css" type="text/css" media="all")

          -#
            %script(src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js")
          %script(src="http://code.jquery.com/jquery-1.9.1.js")
          %script(src="http://code.jquery.com/ui/1.10.3/jquery-ui.js")

          %script(src=ctx.http_uri_prefix+"/js.js" type="text/javascript")
          !=ga()
        %body
          #page_container
            !=o.body
    ');
  }

  static public function body_startpage() {
    // var form_login = new pc.FormExample({ctx:ctx});
    var form = new pc.FormLoginOrCreateAccount({ctx:ctx});
    var ni = function(){ return "no image"; };
    return mw.HTMLTemplate.str('
      !=form.html()
      %h2 users:
      :for(p in User.manager.search(1==1))
        %table
          %tr
            %td User
            %td=p.username
          %tr
            %td name
            %td=p.firstname+p.lastname
          %tr
            %td picture
            %td!=p.picture_html_tag(ctx.image_html_tag, "h-200", ni)
        %hr
    ');
  }

  public static function ga() {
    return "
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-XXXXXXXX-X', 'DOMAIN');
        ga('send', 'pageview');
      </script>";
  }


#end

#end

}
