typedef TranslationFunc =
    String // string to be looked up
    -> Null<String> // additional description for the translator, can be null or ""
    -> String // the translated string
    ;
