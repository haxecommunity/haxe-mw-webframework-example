class Util {

  // function lat_lon_distance_km(a:{lat:Float, lon:Float}, b:{lat:Float, lon:Float}) {
  //   var theta = a.lon - b.lon; 
  //   var dist = Math.sin(Math.deg2rad(a.lat)) * Math.sin(deg2rad(b.lat)) +  Math.cos(deg2rad(a.lat)) * Math.cos(deg2rad(b.lat)) * Math.cos(deg2rad($theta)); 
  //   var dist = Math.acos($dist); 
  //   var dist = Math.rad2deg($dist);
  //   return dist * 60 * 1.85316;
  // }

  static public function lat_lon_distance_km_mysql(lat1, lon1, lat2, lon2) {
	var rad2deg = function(x){ return  '(180.0* (${x}) / PI())'; };
	var deg2rad = function(x){ return  '(PI() * (${x}) / 180.0)'; };
	var dist = 'sin('+deg2rad(lat1)+') * sin('+deg2rad(lat2)+') + cos('+deg2rad(lat1)+') * cos('+deg2rad(lat2)+') * cos('+deg2rad(lon1+'-'+lon2)+')';
	return rad2deg('acos('+dist+')')+' * 60.0 * 1.85316';
  }

}
