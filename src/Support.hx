#if !macro
import haxe.imagemagick.Imagick;
import haxe.imagemagick.ImagickPixel;
#end

using StringTools;

class Support {

  static public function staticImageDimensions() {

    var t_class = "src/KnownImages.hx";
    var t_dump = "image-sizes.dump";

    var images = [];
    var base = "php/pics/";

    for (sourceDir in [""]){
      for(p in sys.FileSystem.readDirectory('${base}${sourceDir == "" ? "" : "/"}${sourceDir}')){
        var ext = haxe.io.Path.extension(p).toLowerCase();
        if (ext == "png" || ext == "jpg"){
          var image = new Imagick('${base}${sourceDir}${sourceDir == "" ? "" : "/"}${p}');
          images.push({
            name: p.replace(".", "_"),
            path: '${sourceDir}${sourceDir == "" ? "" : "/"}${p}',
            w: image.width,
            h: image.height,
          });
        }
      }
    }

    sys.io.File.saveContent(t_dump, haxe.Serializer.run(images));
  }

  static function main() {
    staticImageDimensions();
  }
}

/*


   class Main {

   macro static function dim(path:string){
   ;
 */
