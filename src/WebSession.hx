import SPODs;
using DateTools;
import pc.PC;
import Cfg;

#if !macro

typedef WebSessionType = {
  version: Int,
  ?user: SPODs.User, // not null if logged in
  ?last_action: Date, // for logging out
  // http_referers: RefererCache
}

class WebSession {

#if php
  public static var session_started: Bool = false;
#end
	
  public static function emptySession():WebSessionType {
    return {
      version: 0,
      last_action: Date.now()
      // http_referers: new RefererCache()
    };
  }

  static public function loggedin():Bool {
	return session().user != null;
  }

  static public function assert_loggedin(ctx:Ctx) {
    if (!WebSession.loggedin()) throw new ActionError(ctx.translate("You're no longer logged in!",""));
  }

  static public function session(): WebSessionType {
#if php
	if (!session_started){
	  untyped __call__("session_start", Cfg.projectName);
	  session_started = true;
	  if (untyped __call__("empty", __var__("_SESSION"))){
	   untyped __set__("_SESSION", "ses", emptySession());
	  }
	}
	var s = untyped __var__("_SESSION", 'ses');
#elseif (neko && USE_HAXE_SESSION)
        var s = haxe.Session.get("ses");
        if (s == null){
          save(emptySession());
          s = haxe.Session.get("ses");
        }
        return s;
#else
	TODO
#end
	if (s.last_action == null)
	  s.last_action = Date.now();
    // migrate
    if (s.http_referers == null){
      // s.http_referers = new RefererCache();
    }
	return s;
  }

  static public function auto_logout() {
	var x = session();
	if (x.last_action.getTime() > Date.now().delta(1.hours()).getTime()){
	  logout();
	}
	x.last_action = Date.now();
	save();
  }

  static public function logout() {
    save(emptySession());
  }

  // call this after modifying session
  // only pass s if you want to set a new WebSessionType object
  static public function save(s:WebSessionType = null) {
#if php
	session();
	if (s != null)
	  untyped __set__("_SESSION", "ses", s);
#elseif (neko && USE_HAXE_SESSION)
          haxe.Session.set("ses", s);
#else
	TODO
#end
  }
}
#end
