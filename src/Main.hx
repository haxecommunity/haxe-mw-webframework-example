import web.Server;
import app.schemes.AppScheme;
import SPODs;
import Cfg;

using web.RequestResponseHelper;

class Main {

#if !macro

  static public function log_referer(rr:Ctx) {
    // WebSession.session().http_referers.referers.push(rr.referer());
    // WebSession.save();
  }

  static function main() {

    // initial setup:
    Cfg.setup();

    Server.serve(function(ctx_){

      if (
        Cfg.app_config.dev_error_file != null
        && sys.FileSystem.exists(Cfg.app_config.dev_error_file)
      )
      sys.FileSystem.deleteFile(Cfg.app_config.dev_error_file);

#if CATCH_ALL
      mw.php.CatchAll.use(function(e){
        var s = mw.php.CatchAll.catchAllErrorToString(e);
        if (Cfg.app_config.dev_error_file == null)
          trace(s);
        else
          sys.io.File.saveContent(Cfg.app_config.dev_error_file, s);
      }, function()
      {
#end

        #if AUTO_MIGRATE
        // bootstrapping db, keep first

        if (ctx_.getOrNull("EXCEPTION_TEST") != null){
          throw "exception";
        }

        if (ctx_.getOrNull("AUTO_MIGRATE") != null){
          trace("migrating");
          Maintainance.automigrate(cast(ctx_));
          // add currencies

        }
        #end
        // SPOD setup
        sys.db.Manager.cnx = Cfg.sys_db;


        var domain_and_path = ctx_.domain_and_path();


        {
          var ctx:Ctx = cast(ctx_);
          ctx.translate = function(a,b){ return a; };
          // ctx.http_uri_prefix = ctx_.domain_and_path();
          ctx.http_uri_prefix = 'http://'+Cfg.app_config.domain_and_path;

          ctx.uri_rest = domain_and_path.substr(Cfg.app_config.domain_and_path.length+1);

          ctx.uri_rest_without_get = ctx.uri_rest.split("?")[0];
          ctx.send_email = function(email:Email){
            EmailMailer.send(email);
          };

          // storing / loading / using images:
          ctx.image_store = function(path, content){
            return Image.store_image('pics', path, content);
          };
          ctx.image_url = function(imageData, target_size){ return Image.img_url(ctx, imageData, target_size); };
          ctx.image_html_tag = function(imageData, size){ return Image.img_tag(ctx, imageData, size); };


          ctx.localization = {
            date_to_str: function(d:Date){
              return DateTools.format(d, '%Y-%m-%d');
            },
            datetime_to_str: function(d:Date, secs:Bool = false){
              return DateTools.format(d, '%Y-%m-%d %H:%m}');
            },
            time_to_str: function(d:Date, secs:Bool = false){
              return DateTools.format(d, '%H:%m');
            }
          }

          HTML.ctx = ctx;

          var path = ctx.path();

          if (ctx.uri_rest == "js.js"){
            var language_id = "lang_id"; // js may contain dictionaries for translations, thus create an own js file
            var js_file = 'js-${language_id}.js';
            // need to recreate js_file?
            if (
                !sys.FileSystem.exists(js_file)
                || (
                sys.FileSystem.stat('js-client.js').mtime.getTime()
                > sys.FileSystem.stat(js_file).mtime.getTime()
                )){
              // write everything which is important to the file
              var data = {
                http_uri_prefix: ctx.http_uri_prefix,
              };
              sys.io.File.saveContent(js_file,
                '
                window.data = ${haxe.Json.stringify(data)};
                '+sys.io.File.getContent('js-client.js')
              );
            }
            ctx.end(200, sys.io.File.getContent(js_file));
            // ctx.end_redirect(301, ctx.http_uri_prefix+'/${js_file}');
            return;
          }

          // USER LOGIN SESSION REQUIRED
          WebSession.auto_logout();

          // PC actions
          if (pc.PC.handleAction(ctx, ctx.uri_rest, function(){ log_referer(ctx); })){
            return;
          }

          if (EmailVerification.handle(ctx)){
            return;
          }
          if (PasswordReset.handle(ctx)){
            return;
          }

          switch (ctx.uri_rest) {
            case "style.css": 
              Style.style(ctx);
              return;
            case "about":
              log_referer(ctx);
              ctx.end(200, HTML.page(ctx, {
                title: Cfg.projectName+" Luxusqueen.net, Private Modetreffen für Vorreiter des exklusiven Geschmacks",
                body: "about"
              }));
              return;
            case _:
              // main page
              log_referer(ctx);
              ctx.end(200, HTML.page(ctx, {
                title: Cfg.projectName+" Luxusqueen.net, Private Modetreffen für Vorreiter des exklusiven Geschmacks",
                body: HTML.body_startpage()
              }));
              return;
          }
        }
#if CATCH_ALL
      }); // catchall
#end
    } // serve
    , function(ctx, e, stack){
      ctx.end(503, e+" "+stack);
    }, {
#if js
port : 8080
#elseif JAVA_NANOHTTPD
port : 8080
#else
#end
    });
  }


#end
}
