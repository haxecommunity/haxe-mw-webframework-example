import mw.web.ImageSize;
import Cfg;

#if !macro

#if (neko)
import haxe.imagemagick.Imagick;
import haxe.imagemagick.ImagickPixel;
#end

typedef ImageData = {
  path:String, w:Int, h:Int
}

typedef ImageContent = {
  content:String,
  // with extension you get at least some additional information
  extension:String
}

typedef ImageStore = String -> String -> {w:Int, h:Int}; // yes, StoreImage would be a nicer name, prefering having the same prefix
typedef ImageUrl = ImageData
  -> String // target size, see ImageSize
  -> String;

typedef ImageHtmlTag = 
    ImageData
    -> String  // size information, see ImageSize
    -> String; // returning html tag

// Provide api for storing images and retrieving html tags / urls
// thus in some point in time you can outsource this to other servers to reduce
// load on main server
typedef ImageStorageAPI = {
  image_store: ImageStore, // path, image contents, returning dimensions
  image_url:  ImageUrl,
  image_html_tag: ImageHtmlTag,
};

class Image {

  static public function img_url(rr:Ctx, image: ImageData, target_size, dim: {w:Int, h:Int} = null):String{
    if  (dim == null) dim = ImageSize.calc(image, target_size);
    return rr.http_uri_prefix + "/pics/auto/" + image.path + '?w=${dim.w}&h=${dim.h}';
  }

  static public function img_tag(rr:Ctx, image: ImageData, target_size):String {
    var dim = ImageSize.calc(image, target_size);
    var title = Reflect.field(image, "title");
    var url = img_url(rr, image, target_size, dim);
    return mw.HTMLTemplate.str('%img(src=url width=dim.w+"" height=dim.h+"" title=title)');
  }

#if (SERVER && !macro)
  static public function store_image(base:String, path:String, image:String):{w:Int, h:Int} {
    var store_path = '${base}/${path}';
    #if php
    var img = untyped __call__('imagecreatefromstring', image);
    var size = {
      w: untyped __call__("imagesx", img),
      h: untyped __call__("imagesy", img)
    };
    untyped __call__('imagedestroy', img);
    sys.FileSystem.createDirectory(haxe.io.Path.directory(store_path));
    sys.io.File.saveContent(store_path, image);
    return size;
    #else
      sys.io.File.saveContent(store_path, image);
      var image = new Imagick(store_path);
      return {
        w: image.width,
        h: image.height
      }
    #end
  }
#end
}

#end
