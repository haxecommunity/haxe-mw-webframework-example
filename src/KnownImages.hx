import Cfg;

typedef SImage = {
	name:String,
	path:String,
	w:Int,
	h:Int
  };

class KnownImages {

  static var images:Array<SImage>;

  static public function getImage(name:String) : SImage {

    var dump = "image-sizes.dump";

	if  (null == images){
          if (sys.FileSystem.exists(dump)){
            images = haxe.Unserializer.run(sys.io.File.getContent(dump));
          } else {
            trace('image sizes dump file ${dump} not found');
            images = [];
          }
	}

	for(i in images){
	  if (i.name == name || i.path == name)
		return i;
	}
	throw 'image ${name} unkown';
  }

  macro static public function imageData(name:String) {
	var i = getImage(name);
	var j = { path: i.path, w: i.w, h: i.h};
	return macro $v{j};
  }

  macro static public function imageHTML(rr:ExprOf<Ctx>, name:String, target_size: ExprOf<String>):ExprOf<String> {
	var i = getImage(name);
	var j = { path: i.path, w: i.w, h: i.h};
	return macro{ Image.img_tag(${rr}, $v{j}, $target_size ); };
  }

  macro static public function image_url(rr:ExprOf<Ctx>, name:String, target_size: ExprOf<String>):ExprOf<String> {
	var i = getImage(name);
	var j = { path: i.path, w: i.w, h: i.h};
	return macro{ Image.img_url(${rr}, $v{j}, $target_size ); };
  }
}
